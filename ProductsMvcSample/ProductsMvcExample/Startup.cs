﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProductsMvcExample.Startup))]
namespace ProductsMvcExample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
